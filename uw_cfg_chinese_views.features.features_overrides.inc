<?php

/**
 * @file
 * uw_cfg_chinese_views.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_chinese_views_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: context
  $overrides["context.people_profiles_categories.reactions|block|blocks|uw_cfg_chinese_views-profile_by_type_chinese"] = array(
    'module' => 'uw_cfg_chinese_views',
    'delta' => 'profile_by_type_chinese',
    'region' => 'sidebar_second',
    'weight' => -10,
  );
  $overrides["context.people_profiles_categories.reactions|block|blocks|uw_ct_person_profile-profile_by_type"]["DELETED"] = TRUE;

  // Exported overrides for: views_view
  $overrides["views_view.events_responsive.display|events_archive_page|display_options|title"] = '活动档案';
  $overrides["views_view.events_responsive.display|events_tag_page|display_options|title"] = '活动';
  $overrides["views_view.events_responsive.display|page|display_options|menu|title"] = '活动';
  $overrides["views_view.events_responsive.display|page|display_options|title"] = '活动';
  $overrides["views_view.uw_blog_recent.display|default|display_options|title"] = '最新博客';
  $overrides["views_view.uw_blog_responsive.display|blog_archive_page|display_options|title"] = '博客档案';
  $overrides["views_view.uw_blog_responsive.display|page_1|display_options|defaults|title"] = FALSE;
  $overrides["views_view.uw_blog_responsive.display|page_1|display_options|title"] = '博客';
  $overrides["views_view.uw_blog_responsive.display|page_2|display_options|title"] = '博客话题';
  $overrides["views_view.uw_blog_responsive.display|page_3|display_options|title"] = '相关话题博客';
  $overrides["views_view.uw_blog_responsive.display|page|display_options|defaults|title"] = FALSE;
  $overrides["views_view.uw_blog_responsive.display|page|display_options|menu|title"] = '博客';
  $overrides["views_view.uw_blog_responsive.display|page|display_options|title"] = '博客';
  $overrides["views_view.uw_news_item_pages_responsive.display|page_1|display_options|defaults|title"] = FALSE;
  $overrides["views_view.uw_news_item_pages_responsive.display|page_1|display_options|menu|title"] = '新闻';
  $overrides["views_view.uw_news_item_pages_responsive.display|page_1|display_options|title"] = '新闻';
  $overrides["views_view.uw_news_item_pages_responsive.display|page_2|display_options|title"] = '新闻档案';
  $overrides["views_view.uw_news_item_pages_responsive.display|page_3|display_options|defaults|title"] = FALSE;
  $overrides["views_view.uw_news_item_pages_responsive.display|page_3|display_options|title"] = '新闻';
  $overrides["views_view.uw_people_profile_pages.display|block_1|display_options|fields|view_node|text"] = 'genduo';
  $overrides["views_view.uw_people_profile_pages.display|block_1|display_options|title"] = '认识我们';
  $overrides["views_view.uw_people_profile_pages.display|page_1|display_options|defaults|title"] = FALSE;
  $overrides["views_view.uw_people_profile_pages.display|page_1|display_options|title"] = '人员简介';
  $overrides["views_view.uw_people_profile_pages.display|page_2|display_options|arguments|term_node_tid_depth|title"] = '%1 人员简介';

 return $overrides;
}
