<?php

/**
 * @file
 * uw_cfg_chinese_views.features.inc
 */

/**
 * Implements hook_context_default_contexts_alter().
 */
function uw_cfg_chinese_views_context_default_contexts_alter(&$data) {
  if (isset($data['people_profiles_categories'])) {
    $data['people_profiles_categories']->reactions['block']['blocks']['uw_cfg_chinese_views-profile_by_type_chinese'] = array(
      'module' => 'uw_cfg_chinese_views',
      'delta' => 'profile_by_type_chinese',
      'region' => 'sidebar_second',
      'weight' => -10,
    ); /* WAS: '' */
    unset($data['people_profiles_categories']->reactions['block']['blocks']['uw_ct_person_profile-profile_by_type']);
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_cfg_chinese_views_views_default_views_alter(&$data) {
  if (isset($data['events_responsive'])) {
    $data['events_responsive']->display['events_archive_page']->display_options['title'] = '活动档案'; /* WAS: 'Events' */
    $data['events_responsive']->display['events_tag_page']->display_options['title'] = '活动'; /* WAS: 'Events' */
    $data['events_responsive']->display['page']->display_options['menu']['title'] = '活动'; /* WAS: 'Events' */
    $data['events_responsive']->display['page']->display_options['title'] = '活动'; /* WAS: 'Events' */
  }
  if (isset($data['uw_blog_recent'])) {
    $data['uw_blog_recent']->display['default']->display_options['title'] = '最新博客'; /* WAS: 'Recent blog posts' */
  }
  if (isset($data['uw_blog_responsive'])) {
    $data['uw_blog_responsive']->display['blog_archive_page']->display_options['title'] = '博客档案'; /* WAS: 'Blog posts archive' */
    $data['uw_blog_responsive']->display['page_1']->display_options['defaults']['title'] = FALSE; /* WAS: '' */
    $data['uw_blog_responsive']->display['page_1']->display_options['title'] = '博客'; /* WAS: '' */
    $data['uw_blog_responsive']->display['page_2']->display_options['title'] = '博客话题'; /* WAS: 'Blog Topic' */
    $data['uw_blog_responsive']->display['page_3']->display_options['title'] = '相关话题博客'; /* WAS: 'Related Posts' */
    $data['uw_blog_responsive']->display['page']->display_options['defaults']['title'] = FALSE; /* WAS: '' */
    $data['uw_blog_responsive']->display['page']->display_options['menu']['title'] = '博客'; /* WAS: 'Blog' */
    $data['uw_blog_responsive']->display['page']->display_options['title'] = '博客'; /* WAS: '' */
  }
  if (isset($data['uw_news_item_pages_responsive'])) {
    $data['uw_news_item_pages_responsive']->display['page_1']->display_options['defaults']['title'] = FALSE; /* WAS: '' */
    $data['uw_news_item_pages_responsive']->display['page_1']->display_options['menu']['title'] = '新闻'; /* WAS: 'News' */
    $data['uw_news_item_pages_responsive']->display['page_1']->display_options['title'] = '新闻'; /* WAS: '' */
    $data['uw_news_item_pages_responsive']->display['page_2']->display_options['title'] = '新闻档案'; /* WAS: 'News archive' */
    $data['uw_news_item_pages_responsive']->display['page_3']->display_options['defaults']['title'] = FALSE; /* WAS: '' */
    $data['uw_news_item_pages_responsive']->display['page_3']->display_options['title'] = '新闻'; /* WAS: '' */
  }
  if (isset($data['uw_people_profile_pages'])) {
    $data['uw_people_profile_pages']->display['block_1']->display_options['fields']['view_node']['text'] = 'genduo'; /* WAS: 'Read more' */
    $data['uw_people_profile_pages']->display['block_1']->display_options['title'] = '认识我们'; /* WAS: 'Meet our people' */
    $data['uw_people_profile_pages']->display['page_1']->display_options['defaults']['title'] = FALSE; /* WAS: '' */
    $data['uw_people_profile_pages']->display['page_1']->display_options['title'] = '人员简介'; /* WAS: '' */
    $data['uw_people_profile_pages']->display['page_2']->display_options['arguments']['term_node_tid_depth']['title'] = '%1 人员简介'; /* WAS: '%1 profiles' */
  }
}
